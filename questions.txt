Welcome to the first Git exercise!
Completing these prompts will help you get more comfortable with
Git.

1. What is your favorite color?

    My favorite color is Blue.

***
  Remember to:
    - add
    - commit
    - push
  your answer before answering the next question!
***

2. What is your favorite food?

  I enjoy any/all Mexican food.

3. Who is your favorite fictional character?

  My favorite fictional character is Deadpool. I enjoy his abilities and
  sense of humor.

4. What is your favorite animal?

  My favorite animal is dolphins. They are incredibly smart and
  a sailors best friend.

5. What is your favorite programming language? (Hint: You can always say Python!!)

  My favorite was Java because that was all I knew, but Python
  became a favorite due to the easy syntax.
